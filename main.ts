import {
  GetProductsForIngredient,
  GetRecipes,
} from "./supporting-files/data-access";
import {
  Nurtients,
  NutrientFact,
  Product,
  ProductSupplierWithCheapCost,
  Recipe,
  RecipeSummary,
  SupplierProduct,
} from "./supporting-files/models";
import {
  GetCostPerBaseUnit,
  GetNutrientFactInBaseUnits,
  SumUnitsOfMeasure,
} from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

const sortedNutrients = (nutrients: any): Nurtients => {
  const sortedNutrients = {};
  const sortedNutrientKeys = Object.keys(nutrients).sort();

  sortedNutrientKeys.forEach((key, val) => {
    sortedNutrients[key] = nutrients[key];
  });

  return sortedNutrients;
};

const getProductSupplierWithCheapstCost = (
  product: Product[]
): ProductSupplierWithCheapCost => {
  return product
    .map((productIngredient: Product) => {
      const supplierWithCheapestCost: number[] =
        productIngredient.supplierProducts.map(
          (supplierProduct: SupplierProduct) =>
            GetCostPerBaseUnit(supplierProduct)
        );

      const nutrientFacts: NutrientFact[] = productIngredient.nutrientFacts.map(
        (nutrientFact) => GetNutrientFactInBaseUnits(nutrientFact)
      );

      return {
        cost: Math.min.apply(Math, supplierWithCheapestCost),
        nutrientFacts,
      };
    })
    .reduce((prev, curr) => (prev.cost < curr.cost ? prev : curr));
};

const getRecipeSummary = (recipe: Recipe): RecipeSummary => {
  const result = {
    cheapestCost: 0,
    nutrientsAtCheapestCost: {},
  };

  recipe.lineItems.forEach((items) => {
    const productIngredients = GetProductsForIngredient(items.ingredient);
    const productSupplierWithCheapestCost =
      getProductSupplierWithCheapstCost(productIngredients);

    // Get the total
    result.cheapestCost +=
      productSupplierWithCheapestCost.cost * items.unitOfMeasure.uomAmount;

    //Get the total nutrients
    productSupplierWithCheapestCost.nutrientFacts.forEach(
      (nutrientFact: NutrientFact) => {
        if (!(nutrientFact.nutrientName in result.nutrientsAtCheapestCost)) {
          result.nutrientsAtCheapestCost[nutrientFact.nutrientName] =
            nutrientFact;

          return;
        }

        result.nutrientsAtCheapestCost[
          nutrientFact.nutrientName
        ].quantityAmount = SumUnitsOfMeasure(
          result.nutrientsAtCheapestCost[nutrientFact.nutrientName]
            .quantityAmount,
          nutrientFact.quantityAmount
        );
      }
    );
  });

  //Sort nutrients
  const sortNutrients = sortedNutrients(result.nutrientsAtCheapestCost);
  result.nutrientsAtCheapestCost = sortNutrients;
  
  return result;
};

recipeData.forEach((recipe: Recipe) => {
  recipeSummary[recipe.recipeName] = getRecipeSummary(recipe);
});
/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
